import os

# Device Configuration
TEST_DEVICE= os.environ['DEVICE_SERIAL_ID']
PLATFORM_NAME = 'iOS'
PLATFORM_VERSION = os.environ['DEVICE_OS_VERSION']
WDALOCALPORT = '8100'

# APPIUM_URL = 'http://128.0.0.0:4723/wd/hub'
APPIUM_URL = os.environ['APPIUM_SERVER']

# BundleID of 5Gmark
BundleID_APP = 'com.agence3pp.4Gmark'

# Test Types
FULL_TEST = 'Full test'
SPEED_TEST = 'Speed test'
TEST_BUTTON = '//XCUIElementTypeButton[@name="TEST"]'
INDOOR_TEST = 'location-type-indoor'
OUTDOOR_TEST = 'location-type-outdoor'
CAR_TEST = 'location-type-car'
TRAIN_TEST = 'location-type-train'

# Result Page
RESULT_PAGE = 'Results'
EXPLANATION_PAGE = 'Explanation'

SCORE_RESULT = '//*[starts-with(@name, "My Score")]'
DOWNLOAD_RESULT = '//*[starts-with(@name, "My download bitrate")]'
UPLOAD_RESULT = '//*[starts-with(@name, "My upload bitrate")]'
STREAM_RESULT = '//*[starts-with(@name, "No lag did")]'
WEB_RESULT = '//*[starts-with(@name, "Average time to load")]'

# Different Tabs
PODIUM_MAP_TAB = '//XCUIElementTypeTabBar[@name="Tab Bar"]/XCUIElementTypeButton[1]'
HISTORY_TAB = '//XCUIElementTypeTabBar[@name="Tab Bar"]/XCUIElementTypeButton[2]'
TEST_TAB = '//XCUIElementTypeTabBar[@name="Tab Bar"]/XCUIElementTypeButton[3]'
MONITORING_TAB = '//XCUIElementTypeTabBar[@name="Tab Bar"]/XCUIElementTypeButton[4]'
ACCOUNT_SETTINGS_TAB = '//XCUIElementTypeTabBar[@name="Tab Bar"]/XCUIElementTypeButton[5]'

BACK_OPTION = '//XCUIElementTypeNavigationBar[@name="_gmark_crowd.GMCResultsPageView"]/XCUIElementTypeButton'

FIRST_RECORD_HISTORY_TAB = '//XCUIElementTypeApplication[@name="5gmark"]/XCUIElementTypeWindow[1]//XCUIElementTypeOther[1]/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther'

MBps_OPTION = 'MBps'
SECOND_OPTION = 'Mbps'
MBps_RESULT= '//*[contains(@name,"MBps")]'
