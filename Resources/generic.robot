*** Keywords ***

LAUNCH 5GMARK APPLICATION
    [Arguments]  ${test_name}=Test  ${device_name}=None
    Open Application  ${APPIUM_URL}  platformName=${PLATFORM_NAME}  platformVersion=${PLATFORM_VERSION}  deviceName=${TEST_DEVICE}  app=${BundleID_APP}  udid=${TEST_DEVICE}  automationName=XCUITest  newCommandTimeout=60000  useNewWDA=false  autoDismissAlerts=true

PRINT ALL RESULT  
    ${score}  get text  ${SCORE_RESULT}
    log to console  ${score}    
    ${download}  get text  ${DOWNLOAD_RESULT}
    log to console  ${download}
    ${upload}  get text  ${UPLOAD_RESULT}
    log to console  ${upload}
    ${stream}  get text  ${STREAM_RESULT}
    log to console  ${stream}
    ${web}  get text  ${WEB_RESULT}
    log to console  ${web}  

PRINT RESULT   
    ${download}  get text  ${DOWNLOAD_RESULT}
    log to console  ${download}
    ${upload}  get text  ${UPLOAD_RESULT}
    log to console  ${upload}