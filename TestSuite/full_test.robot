*** Settings ***
Library         AppiumLibrary
Resource        ${EXECDIR}/Resources/generic.robot
Variables       ${EXECDIR}/VariableFiles/config.py

*** Test Cases ***

TC-001: Indoor Full Test
    [Tags]  IndoorFullTest
    LAUNCH 5GMARK APPLICATION
    builtin.sleep  15
    wait until page contains element  ${FULL_TEST}  timeout=10
    click element  ${FULL_TEST}
    wait until page contains element  ${TEST_BUTTON}  timeout=5
    click element  ${TEST_BUTTON}
    wait until page contains element  ${INDOOR_TEST}  timeout=5
    click element  ${INDOOR_TEST}
    wait until page contains element  ${RESULT_PAGE}  timeout=240
    click element  ${EXPLANATION_PAGE}
    builtin.sleep  5
    PRINT ALL RESULT
    click element  ${BACK_OPTION}
    click element  ${HISTORY_TAB}
    wait until page contains element  ${FIRST_RECORD_HISTORY_TAB}  timeout=15
    click element  ${FIRST_RECORD_HISTORY_TAB}
    ${RESULT_PAGE}  run keyword and return status  wait until page contains element  ${RESULT_PAGE}  timeout=10
    quit application