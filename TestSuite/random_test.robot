*** Settings ***
Library         AppiumLibrary
Resource        ${EXECDIR}/Resources/generic.robot
Variables       ${EXECDIR}/VariableFiles/config.py

*** Test Cases ***

TC-001: Changing Data Unit Mbps To MBps	
    [Tags]  ChangeDataUnits
    LAUNCH 5GMARK APPLICATION
    builtin.sleep  15
    wait until page contains element  ${ACCOUNT_SETTINGS_TAB}  timeout=10
    click element  ${ACCOUNT_SETTINGS_TAB}
    wait until page contains element  ${MBps_OPTION}  timeout=10
    click element  ${MBps_OPTION}
    click element  ${TEST_TAB}
    click element  ${SPEED_TEST}
    click element  ${TEST_BUTTON}
    wait until page contains element  ${INDOOR_TEST}  timeout=5
    click element  ${INDOOR_TEST}
    wait until page contains element  ${RESULT_PAGE}  timeout=240
    click element  ${EXPLANATION_PAGE}
    builtin.sleep  5
    PRINT RESULT
    click element  ${RESULT_PAGE}
    ${get_result}  get text  ${MBps_RESULT}
    should match regexp  ${get_result}  MBps
    log to console  ${get_result}
    log to console  Reverting changes
    click element  ${BACK_OPTION}
    click element  ${ACCOUNT_SETTINGS_TAB}
    wait until page contains element  ${MBps_OPTION}  timeout=10
    click element  ${SECOND_OPTION}
    quit application

TC-002: Outdoor Speed Test  
    [Tags]  OutdoorSpeedTest
    LAUNCH 5GMARK APPLICATION
    builtin.sleep  15
    wait until page contains element  ${SPEED_TEST}  timeout=10
    click element  ${SPEED_TEST}
    wait until page contains element  ${TEST_BUTTON}  timeout=5
    click element  ${TEST_BUTTON}
    wait until page contains element  ${OUTDOOR_TEST}  timeout=5
    click element  ${OUTDOOR_TEST}
    wait until page contains element  ${RESULT_PAGE}  timeout=240
    click element  ${EXPLANATION_PAGE}
    builtin.sleep  5
    PRINT RESULT 
    quit application